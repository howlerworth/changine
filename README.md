# changine

A proof of concept multi-language capable search engine for 4chan boards
using:

* xapian as the search engine
* langdetect for language detection
* basc_py4chan as the interface to 4chan's API
* basc_archiver as the crawler

Current status: Early Doors :)

It currently only assumes that an existing xapian database exists.

The Flask-powered web interface has a number of endpoints:
* `/` presents basic information about the Xapian database
* `/search` provides a basic search interface
* `/docid/<int:id>` provides a view into the Xapian document details
* `/thread/<int:thread>` provides all messages for a given OP thread ID

## TODO
- Use `basc_archiver` as an ingestion source for changine
- A mechanism for adding threads to `basc_archiver` while changine is
  running

## Requirements
* Python 3+
* xapian's Python bindings. Check [xapian's download page](https://xapian.org/download)
  for your distributions settings

## Why Xapian?
* xapian is a search engine library that offers multilingual support and
  and [omega](https://xapian.org/docs/omega/overview.html), a easy way
  to get a website for performing searches
* It's low maintenance and low overhead compared to other projects like
  Elasticsearch

### Document Structure
Each 4chan post is stored as its own [Xapian document](https://getting-started-with-xapian.readthedocs.io/en/latest/concepts/indexing/documents.html)  
with references to its OP. The post's JSON is stored as the document's
data. The document [values](https://getting-started-with-xapian.readthedocs.io/en/latest/concepts/indexing/values.html)  
are currently:
* The post id of the OP
* The post timestamp
* The post date (YYYYMMDD format)

Indexed terms are the title and comment of a post which are also free
text indexed. This means you can search specifically for text in the
title or comment as well as generic searches that will search anywhere.

Filterable terms (boolean terms in Xapian terminology) are:
* The board name
* OP's post ID
* The post language as detected by `langdetect`

## Running and Searching
1. Install the dependencies from `requirements.txt` and then `python3 changine.py`
2. You'll notice that after it finishes, you have a `xapian` directory
3. `xapian-delve` is a utility allowing basic search and debug
   capabilities against the generated database

### Examples
* Searching for "strange" things in Italian
```
$ xapian-delve -s it -d -t Zstran  xapian
Posting List for term 'Zstran' (termfreq 2, collfreq 2, wdf_max 1): 8000 [url=http://boards.4chan.org/int/thread/124763747#p124768562
sample=>>124768334
Cinisello è una città in cui succedono cose strane
caption=None
author=Anonymous

created=1591649660

<a href="#p124768334" class="quotelink">&gt;&gt;124768334</a><br>Cinisello è una città in cui succedono cose strane
] 8056 [url=http://boards.4chan.org/int/thread/124763747#p124771569
sample=Che strano vedere persone nere in occidente che adottano bianchi
https://youtu.be/wBzVEzJcStQ
caption=None
author=Anonymous

created=1591653133

Che strano vedere persone nere in occidente che adottano bianchi<br>https://youtu.be/wBzVEzJcStQ
```

* Searching for Hungarian language posts
```
...
sample=>>124732207                                                                                                                                                                                                  
hol dolgozol, hogy négerek is vannak? helyeske van köztük?                                                                                                                                                          
caption=None                                                                                              
author=anonymous                                                                                                                                                                                                    
                                                                                                          
created=1591606492                                                                                                                                                                                                  
                                                                                                                                                                                                                    
<a href="#p124732207" class="quotelink">&gt;&gt;124732207</a><br>hol dolgozol, hogy négerek is vannak? helyeske van köztük?                                                                                                                                                                                                                                                                                                              
] 5263 [url=http://boards.4chan.org/int/thread/124731819#p124732483                                                                                                                                                 
sample=>>124732207                                                                                                                                                                                                  
t. bölcsész excelmajom multi szolgáltatóközpontban?                                                                                                                                                                                                                                                                                                                                                                                      
caption=None                                                                                              
author=anonymous                                                                                          
                                                                                                                                                                                                                    
created=1591606568                                                                                                                                                                                                  
                                                                                                                                                                                                                    
<a href="#p124732207" class="quotelink">&gt;&gt;124732207</a><br>t. bölcsész excelmajom multi szolgáltatóközpontban?                                                                                                                                                                                                                                                                                                                     
] 5264 [url=http://boards.4chan.org/int/thread/124731819#p124732568                                                                                                                                                 
sample=>>124732483                                                                                                                                                                                                  
seggfájós tanyasi már megint a niggerek miatt rinyál                                                                                                                                                                
caption=None                                                                                                                                                                                                        
author=anonymous                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                         
created=1591606719                                                                                                                                                                                                  
                                                                                                          
<a href="#p124732483" class="quotelink">&gt;&gt;124732483</a><br>seggfájós tanyasi már megint a niggerek miatt rinyál                                                                                               
] 5267 [url=http://boards.4chan.org/int/thread/124731819#p124732689                                                                                                                                                 
sample=>>124732483                                                                                                                                                                                                                                                                                                                                                                                                                       
fúj bazdmeg én majdnem az lettem                                                                                                                                                                                    
áldom is a sorsom hogy máshogy alakult, lehet már nem élnék                                               
caption=None                                                                                              
author=anonymous                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                          
created=1591606948                                                                                        
                                                                                                                                                                                                                    
<a href="#p124732483" class="quotelink">&gt;&gt;124732483</a><br>fúj bazdmeg én majdnem az lettem<br>áldom is a sorsom hogy máshogy alakult, lehet már nem élnék                                                    
] 5268 [url=http://boards.4chan.org/int/thread/124731819#p124732702                                                                                                                                                 
sample=több mint három héttel ezelőtt rendeltem magamnak 70 kilónyi tárcsasúlyt az itthoni kézisúlyzóimhoz, erre ma írnak nekem, hogy sajnos a kívá                                                                                                                                                                                                                                                                                      
caption=None                                                                                              
author=anonymous                                                                                                                                                                                                    
                                                                                                          
created=1591606975                                                                                        
                                                                                                          
több mint három héttel ezelőtt rendeltem magamnak 70 kilónyi tárcsasúlyt az itthoni kézisúlyzóimhoz, erre ma írnak nekem, hogy sajnos a kívánt termékből egy darab nem maradt már európában, talán leghamarabb augusztus végére tudnák teljesíteni a rendelésemet, úgyhogy ha nem baj, ők most törlik is a rendelési igényemet, legfeljebb nézzek vissza az oldalukra pár hét múlva, hátha addigra megoldódik a készlethiány.<br>ánonnak 
esetleg van tippje, honnan rendelhetnék még itthonról súlyokat?                                           
] 5269 [url=http://boards.4chan.org/int/thread/124731819#p124732719                                       
sample=>>124732702                                                                                                                                                                                                  
szar tészta                                                                                               
kurva anyád                                                                                               
caption=None                                                                                                                                                                                                        
author=anonymous                                                                                          
                                                                                                                                                                                                                    
created=1591607012                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                                                                                                                                                                                    
<a href="#p124732702" class="quotelink">&gt;&gt;124732702</a><br>szar tészta<br>kurva anyád
...
```
