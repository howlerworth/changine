import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    XAPIAN_DATABASE = '~/PycharmProjects/changine/xapian'
    XAPIAN_LANGS = (
        ('ar', 'Arabic'),
        ('hy', 'Armenian'),
        ('eu', 'Basque'),
        ('ca', 'Catalan'),
        ('da', 'Danish'),
        ('nl', 'Dutch'),
        ('en', 'English'),
        ('fi', 'Finnish'),
        ('fr', 'French'),
        ('de', 'German'),
        ('hu', 'Hungarian'),
        ('id', 'Indonesian'),
        ('ga', 'Irish'),
        ('it', 'Italian'),
        ('lt', 'Lithuanian'),
        ('ne', 'Nepali'),
        ('nb', 'Norwegian'),
        ('nn', 'Norwegian'),
        ('no', 'Norwegian'),
        ('pt', 'Portuguese'),
        ('ro', 'Romanian'),
        ('ru', 'Russian'),
        ('es', 'Spanish'),
        ('sv', 'Swedish'),
        ('ta', 'Tamil'),
        ('tr', 'Turkish'),
    )
    SECRET_KEY = '*4xsfrlv11(4S&#aWjRhbC0.JWnqRejW'
    PAGE_SIZE = 10
