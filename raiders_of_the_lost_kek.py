import json
from datetime import datetime

import xapian
import io
import zstandard

from langdetect import DetectorFactory
from langdetect.lang_detect_exception import LangDetectException

db = xapian.WritableDatabase('xapian',
                             xapian.DB_CREATE_OR_OPEN)
termgenerator = xapian.TermGenerator()

df = DetectorFactory()
df.load_profile('~/langdetect/langdetect/profiles')

XAPIAN_LANGS = ('ar', 'hy', 'eu', 'ca', 'da', 'nl', 'en', 'fi', 'fr', 'de',
                'hu', 'id', 'ga', 'it', 'lt', 'ne', 'nb', 'nn', 'no', 'pt',
                'ro', 'ru', 'es', 'sv', 'ta', 'tr')

with open('~/pol_062016-112019_labeled.ndjson.zst', 'rb') as fh:
    x = 0
    dctx = zstandard.ZstdDecompressor()
    stream_reader = dctx.stream_reader(fh, read_across_frames=True)
    text_stream = io.TextIOWrapper(stream_reader, encoding='utf-8')
    for line in text_stream:
        op = None
        temp = json.loads(line)

        for post in temp['posts']:
            detect = df.create()
            if post.get('replies'):
                op = post['no']
            if post.get('com'):
                x += 1
                if (x % 5000) == 0:
                    print(str(x) + '\r', end='')
                try:
                    detect.append(post.get('com'))
                    suspected_lang = detect.detect()
                except LangDetectException as e:
                    suspected_lang = 'en'

                if suspected_lang in XAPIAN_LANGS:
                    termgenerator.set_stemmer(xapian.Stem(suspected_lang))
                    doc = xapian.Document()
                    termgenerator.set_document(doc)

                    # Index the subject and comment
                    if post.get('sub'):
                        termgenerator.index_text(post['sub'], 1, 'S')
                    if post.get('com'):
                        termgenerator.index_text(post['com'], 1, 'XD')

                    # Free text search the subject and comment
                    if post.get('sub'):
                        termgenerator.index_text(post['sub'])
                        termgenerator.increase_termpos()
                    if post.get('com'):
                        termgenerator.index_text(post['com'])

                    # timestamp is epoch
                    posted = datetime.fromtimestamp(post['time'])

                    # Set up the boolean terms for faceted search
                    idterm = u'Q{}'.format(post['no'])
                    boardname = u'Gpol'
                    lang = u'L{}'.format(suspected_lang)

                    date_ = u'D{}'.format(posted.strftime('%Y%m%d'))
                    xop = u'XOP{}'.format(op)

                    # Get the possible authors
                    authors = []
                    if post.get('trip'):
                        authors.append(post['trip'].lower())

                    if post.get('name'):
                        authors.append(post['name'].lower())

                    # Add the boolean terms to the record
                    for author in authors:
                        doc.add_boolean_term(u'A' + author)
                    doc.add_boolean_term(idterm)
                    doc.add_boolean_term(boardname)
                    doc.add_boolean_term(lang)
                    doc.add_boolean_term(xop)
                    # Pre-set the JSON to be pretty printed for later
                    doc.set_data(json.dumps(post, sort_keys=True, indent=4))

                    if not op:
                        op = 0
                    doc.add_value(1, xapian.sortable_serialise(op))
                    doc.add_value(2, xapian.sortable_serialise(post['time']))
                    doc.add_value(3, posted.strftime('%Y%m%d'))

                    db.replace_document(idterm, doc)
