from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Optional


class SearchForm(FlaskForm):
    phrase = StringField('Phrase', validators=[DataRequired()])
    from_ = DateField('From', validators=[Optional()])
    to = DateField('To', validators=[Optional()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
