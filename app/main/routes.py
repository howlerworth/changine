import json
from datetime import datetime

import xapian
from flask import current_app, g, make_response, redirect, render_template, \
    request, url_for
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers.data import JsonLexer

from app import db
from app.main import bp
from app.main.forms import SearchForm


TERM_BLACKLIST = ('http', 'com', 'bump', 'www')


class SuggestedTermFilter(xapian.ExpandDecider):
    def __call__(self, term):
        return term[:3] == b'ZXD'


@bp.before_app_request
def before_request():
    g.search_form = SearchForm()


@bp.route('/')
@bp.route('/index')
def index():
    # Return basic information about the collected threads/posts
    num_docs = db.get_doccount()

    return render_template(
        'index.html',
        num_docs=num_docs
    )


@bp.route('/docid/<int:docid>')
def get_doc_details(docid: int):
    doc: xapian.Document = db.get_document(docid)

    # Use pygments to pretty-print the json and use HTML5 code wrap
    html_formatter = HtmlFormatter(style='colorful', wrapcode=True)

    doc_ = dict(docid=docid,
                num_terms=doc.termlist_count(),
                json=highlight(doc.get_data().decode('utf-8'),
                               JsonLexer(), html_formatter),
                termlist=[term for term in doc],
                styles=html_formatter.get_style_defs()
                )
    return render_template('docid.html', doc=doc_)


@bp.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)

    # Build out our query and query
    queryparser = xapian.QueryParser()
    queryparser.set_stemmer(xapian.Stem('en'))
    queryparser.set_stemming_strategy(queryparser.STEM_SOME)
    queryparser.add_prefix('title', 'S')

    # Get the date range processing going
    queryparser.add_rangeprocessor(
        xapian.DateRangeProcessor(3, xapian.RP_DATE_PREFER_MDY)
    )

    phrase = g.search_form.phrase.data
    start_date = g.search_form.to.data
    end_date = g.search_form.from_.data

    _query = ''
    if start_date and end_date:
        _query += '{}..{}'.format(start_date, end_date)
    elif start_date and not end_date:
        _query += '{}..'.format(start_date)
    elif not start_date and end_date:
        _query += '..{}'.format(end_date)

    if _query and phrase:
        query = queryparser.parse_query('{} {}'.format(phrase, _query))
    else:
        query = queryparser.parse_query(g.search_form.phrase.data)

    enquire: xapian.Enquire = xapian.Enquire(db)
    enquire.set_query(query)
    matches: xapian.MSet = enquire.get_mset(
        (page - 1) * current_app.config['PAGE_SIZE'],
        current_app.config['PAGE_SIZE'])
    upper_bound: int = matches.get_matches_upper_bound()

    # Get some suggested terms
    rset = xapian.RSet()
    for match in enquire.get_mset(0, 10):
        rset.add_document(match.docid)
    eset = enquire.get_eset(20, rset, SuggestedTermFilter())
    temp_suggested_terms = set([res.term[3:].decode('utf-8') for res in eset])
    for search_term in query:
        temp_suggested_terms.add(search_term.decode('utf-8')[1:])
    temp_suggested_terms = [term for term in temp_suggested_terms if
                            term not in TERM_BLACKLIST and 'http' not in term]
    suggested_terms = list(temp_suggested_terms)[0:10]

    # Get some results
    parsed_matches = []
    match: xapian.MSetItem
    for match in matches:
        j = json.loads(match.document.get_data().decode('utf-8'))
        t = {
            'rank': match.rank,
            'percent': match.percent,
            # Generate a preview of the search term in the message text
            'snippet': matches.snippet(j['com']).decode('utf-8'),
            'date': datetime.fromtimestamp(j['time']).strftime('%Y-%m-%d'),
            'op': int(xapian.sortable_unserialise(match.document.get_value(1)))
        }
        parsed_matches.append(t)

    next_url = url_for('main.search',
                       phrase=g.search_form.phrase.data,
                       to=g.search_form.to.data,
                       from_=g.search_form.from_.data,
                       page=page + 1 if (page * current_app.config['PAGE_SIZE']) <= upper_bound else None)
    prev_url = url_for('main.search',
                       phrase=g.search_form.phrase.data,
                       to=g.search_form.to.data,
                       from_=g.search_form.from_.data,
                       page=page - 1)

    return render_template('search.html',
                           matches=parsed_matches,
                           next_url=next_url,
                           prev_url=prev_url,
                           to=g.search_form.to.data,
                           from_=g.search_form.from_.data,
                           suggested_terms=suggested_terms)


@bp.route('/thread/<int:thread>')
def op(thread: int):
    """
    Render a thread and its posts. The threads are set with a document slot
    whose value is the OP post number. In order to preserve the original thread
    ordering, the messages are sorted by the submission datetime
    :param thread: 4chan thread to be fetched
    :return:
    """
    queryparser: xapian.QueryParser = xapian.QueryParser()
    queryparser.add_prefix('thread', 'XOP')
    query: xapian.Query = queryparser.parse_query('thread:{}'.format(thread))

    enquire = xapian.Enquire(db)
    enquire.set_query(query)
    # Sort the posts by datetime posted descending
    enquire.set_sort_by_value(2, False)

    # It is unknown how many messages will be in a thread, but since there are
    # limits on the number of posts allowed, just load them all.
    matches: xapian.MSet = enquire.get_mset(0, db.get_doccount())
    match: xapian.MSetItem
    match_data = []
    for match in matches:
        post = json.loads(match.document.get_data().decode('utf-8'))
        # The time attribute in the post is epoch, but since we'll use the
        # HTML5 <time> element, convert to the equivalent UTC timestamp
        post['datetime'] = datetime.fromtimestamp(post['time']).strftime("%Y-%m-%d %H:%M:%S.%fZ")
        post['docid'] = match.docid
        match_data.append(post)
    return render_template('_thread.html', thread=match_data)
